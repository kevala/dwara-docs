===========
Definitions
===========

Action 
    An operation that Dwara can perform

Artifact
    The basic unit of data managed by Dwara

Artifact Class
    A (disjoint) set of artifacts of a similar nature

File
    A normal file or directory

Finalize
    A utility task used to close a physical volume so that no more data can be written on it

Flow
    A composition of interdependent tasks and flows

Flow Element
    An individual component of a flow

Group Volume
    A logical grouping (pool) of physical volumes

Initialize
    A utility task used to prepare a new physical volume before it can be used

Job
    An instance of a task

Marked Completed
    Job status used to indicate the job is not possible or no longer applicable 

Marked Failed
    Job status used to indicate the job had completed, but an administrator has identified a problem with it 

Physical Volume
    A physical unit of long term storage

Processing Task
    A task that processes certain types of files

Provisioned Volume
    A unit of long term storage provided by a service external to Dwara

Query
    A synchronous action 

Queued
    Job status used to indicate a job is waiting for resources to run

Request
    An instruction to perform an action

Requeue
    A query used to set a job's status to queued

Restore
    A storage task that restores a file from a volume

Sequence
    A construct for assigning a set of unique codes 

Storage Level
    The 

Storage Task
    A write or restore task

Storage Type
    A mode of storage (tape, disk, cloud)

Task
    An asynchronous action

Utility Task
    An inbuilt task that performs some common useful function

Verify
    A processing task used to verify the checksum of a file

Volume
    An abstraction for a unit of long term storage

Write
    A storage task that writes an artifact to a volume