===============
Database Schema
===============

This is the db schema docs

flow
~~~~

.. csv-table:: 
   :header: "Field", "Type", "Description"
   :widths: 7, 3, 30
   
   "id", "varchar", "Flow id (should be a meaningful name)."
   "config", "json", "Flow configuration options."


artifactclass
~~~~~~~~~~~~~

A class of artifacts that share common characteristics. This grouping is done in order to apply similar processing or storage policies to all artifacts in a class. An artifact must be in exactly one artifact class.

.. csv-table:: 
   :header: "Field", "Type", "Description"
   :widths: 7, 3, 30
   
   "id", "varchar", "Artifactclass id (should be a meaningful name)."
   "source", "boolean", "Whether the artifacts are original (source) data, or they are derived from other artifacts via some processing (e.g. video transcoding)."
   "artifactclass_ref_id", "varchar", "If the artifactclass is derived from another artifactclass *(source = false)*, this field refers to the source artifactclass. [Optional]"


