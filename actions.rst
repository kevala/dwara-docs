
.. _actions:

=======
Actions
=======

------------
Sync Actions
------------

.. csv-table::
   :header: "Action", "Request", "Description"
   :widths: 7, 4, 25

   "scan", "no", "List and validate the files in the users' ingest directories. This is an important step to perform before doing an ingest."
   "list", "no", "List a set of artifacts or files based on some filter criteria." 
   "rename_staged", "yes", "Rename a staged artifact."
   "cancel", "yes", "Cancel a queued system request."
   "requeue", "yes", "Requeue a failed job. Simply resets the job's status to 'queued'. The admin should have addressed the reason for the job failure so that it should succeed in the subsequent run." 