.. dwara documentation master file, created by
   sphinx-quickstart on Fri Apr  9 12:11:25 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. meta::
   :description: Dwara is an open source long term digital preservation framework written in Java, based on pluggable storage and processing modules

=================
Welcome to Dwara!
=================

**Dwara is an open source digital preservation framework written in Java, based on pluggable storage and processing modules**

.. toctree::
   :maxdepth: 1

   intro
   core-concepts
   actions
   tasks
   flows
   requests
   links
   examples
   definitions
   api
   db-schema


