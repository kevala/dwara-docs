=============
Core Concepts
=============

.. toctree::
    :maxdepth: 1

---------
Artifacts
---------

Dwara manages data in units referred to as **artifacts**. An artifact may be a directory or a file, and should be a natural, independent unit of business data. Artifacts are created by ingesting or importing directories/files into the system. 

Similar kinds of artifacts are grouped into **artifact classes**, with each artifact belonging to exactly one artifact class. An artifact class must have a **sequence scheme** associated with it. This is a construct that automatically assigns a unique code to each artifact in the class. 

-------
Actions
-------

Actions are the operations that Dwara can perform and can be one of the following types: **query**, **task**, or **flow**. 

^^^^^^^
Queries
^^^^^^^

Queries are operations that use a synchronous API call, and which should take no more than a few seconds to complete. They include operations that fetch information, perform certain file system operations, and set database flags.

^^^^^
Tasks
^^^^^

Tasks are asynchronous operations (take some time to complete), and are managed by Dwara's job queuing system. Tasks can be one of the following types: **storage**, **processing**, or **utility**.

Storage Task
    A task that writes or restores data to or from storage volumes.

Processing Task
    A task that processes certain types of files, often generating other files as output. Dwara is designed to be extended with custom processing tasks.

Utility Task
    An administrative or commonly used task provided by the framework.

See :ref:`tasks` for more details.

^^^^^
Flows
^^^^^

A **flow** is a set of interdependent tasks and (sub)flows. Custom flows can be defined for automating any activity :


See :ref:`actions` for more details.
  

Requests
========
*A request is an instruction to perform an action.*


