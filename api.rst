===
API
===

Queries
+++++++

Artifact Class
~~~~~~~~~~~~~~

**GET /artifactclass/{artifactClassId}**

Get details of the specified artifact class

*Example response body* ::

   {
      "artifactclass": "video-edit-pub",
      "description": "Public edited video",
      "pathPrefix": "/data/dwara/staged",
      "source": true,
      "artifactclassRefId": null,
      "importOnly": false,
      "config": { 
         "pathnameRegex": "[^/]+|Output/[^/]+\\.mov"
      },
      "volumes": [
         { 
            "type": "group",
            "volume": "E1",
            "active": true
         },
         { 
            "type": "group",
            "volume": "E2",
            "active": true
         }
      ]
   }


     



**GET /artifactclass**

Get details of all artifact classes

*Example response body* ::

   [
      {
         "artifactclass": "video-edit-tr-pub",
     


Scan Staged Artifacts
~~~~~~~~~~~~~~~~~~~~~

**GET /staged/scan**

Scan the users' ingest directories for artifacts to be ingested. Artifacts are returned grouped by artifact class

*Example response body* ::

   [
      {
         "artifactclass": "video-edit-tr-pub",
         "volumeGroup": [
            "E1",
            "E2",
            "E3"
         ],
         "artifactTotalCount": 1,
         "artifact": [
            {
               "path": "/data/dwara/user/swamikevala/ingest/video-edit-tr-pub",
               "user": "swamikevala",
               "name": "SGChiYT002024_Can-Wearing-Gemstones-Change-My-Life-and-Destiny_Hindi_05-Jan-2021",
               "fileCount": 10,
               "totalSize": 127576291,
               "errors": []
            }
         ]
      },
      {
         "artifactclass": "video-edit-tr-priv1",
         "volumeGroup": [
            "E1",
            "E2",
            "E3"
         ],
         "artifactTotalCount": 1,
         "artifact": [
            {
               "path": "/data/dwara/user/swamikevala/ingest/video-edit-tr-priv1",
               "user": "swamikevala",
               "name": "Yantra Cermony-Online Sadhana_Telugu_12-Apr-2021",
               "fileCount": 63,
               "totalSize": 9142541233,
               "errors": [
                  {
                     "type": "Error",
                     "message": "Artifact Name contains special characters"
                  }
               ]
            }
         ]
      }
   ]

Cancel Request
~~~~~~~~~~~~~~

**POST /request/{requestId}/cancel** 

Cancel the specified request

Release Request
~~~~~~~~~~~~~~~

**POST /request/{requestId}/release**

Release the specified (held) request

Mark Job Failed
~~~~~~~~~~~~~~~

**POST /job/{jobId}/mark_failed** 

Mark the specified job as failed. This should be used in the case where a job has completed (or been marked completed), and subsequently an administrator sees that there was a problem with job. This could for example be due to a bug in a processing task. All dependent jobs will automatically be marked failed also.

A marked failed job can be requeued (like a failed job), and upon completion any dependent jobs are automatically requeued.

Flows
+++++

The flow API can be called for artifacts or volumes. When called for a volume, the flow is applied to all artifacts on the volume. The dynamic *copy* property is automatically set based on the volume id.

**/artifact/{artifactId}/flow/{flowId}**

**/volume/{volumeId}/flow/{flowId}**

**/file/{fileId}/flow/pfr**

ingest
~~~~~~

**POST /staged/ingest**

Ingest the specified files into the system.   

*Example request body* ::

   {
      "stagedFiles": [
         {
            "artifactclass": "video-pub",
            "path": "/data/user/ramkumarj/video-pub",            
            "name": "10058_Guru-Pooja-Offerings-Close-up-Shot_AYA-IYC_15-Dec-2019_X70_9",       
         },
         {
            "artifactclass": "video-priv1",           
            "path": "/data/user/naveeng/video-priv1",
            "name": "shivaShambho"
         }
      ]
   }

*Example response body* ::

   {
      "userRequestId": 808,
      "action": "ingest",
      "requestedBy": "swami.kevala@ishafoundation.org",
      "requestedAt": "19-12-2020T17:51Z",
      "systemRequests": [
         {
            "id": 809,
            "stagedFilePath": "/data/user/ramkumarj/pub-video",
            "skippedActionElements": [5, 8, 11],
            "rerunNo": 0,
            "artifact": {
               "artifactId": 810,
               "artifactclass": "pub-video",
               "sequenceCode": "10058",
               "prevSequenceCode": null,
               "name": "10058_Guru-Pooja-Offerings-Close-up-Shot_AYA-IYC_15-Dec-2019_X70_9",
               "fileCount": 555,
               "totalSize": 16762189,
               "deleted": false,
               "md5": "12385b69040eee0ff216a03195a9836c",
               "artifactIdRef": 0
            }
         },
         ...
      ]
   }

rewrite
~~~~~~~

**POST /volume/R10010L7/rewrite [202]**

Rewrite a volume using one of 3 modes: *replace*, *migrate* or *copy*. 

**replace**
   
This is used to rewrite a defective volume. All artifacts are restored from the specified source copy (which must be a different copy) and are then written to the same volume group as the volume specified in the URL. The rewritten physical artifacts are soft deleted.

*Example request body* ::

   {
      "mode": "replace",
      "sourceCopy": 2   
   }

**migrate**

This is used to migrate the data to one or more new volumes, usually because the volume is deemed to be nearing its end of life or is in danger of becoming obsolete. All artifacts are restored from the volume and are then rewritten to the same volume group. The original physical artifacts are marked as "migrated". 

*Example request body* ::

   {
      "mode": "migrate"
   }

**copy**

This is used to make sure that all artifacts on the volume are written to the specified copy. An error will be returned if the corresponding mapping is not defined in the *artifactclass-to-volumegroup* configuration. The system will skip any artifacts which already exist on the copy (e.g. if the config was changed to increase the number of copies while tapes were partially written). Only artifacts in the matching artifact classes will be copied. 

*Example request body* ::

   {
      "mode": "copy",
      "destinationCopy": 4,
      "artifactclassRegex": "photo_\.+",
   }

   
