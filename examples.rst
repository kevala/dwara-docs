=============
Example Flows
=============

Here we give some examples of possible Dwara configurations

-------
rewrite
-------

This flow is used for rewriting a defective volume.

API: **/volume/A1085L7/flow/rewrite**

**Flow Element Configuration**

.. csv-table::
    :header: "Id", "Dependencies", "Task Type", "Task", "Config" 
    :widths: 5, 5, 5, 5, 20

    "1", "", "storage", "restore", ""
    "2", "[1]", "processing", "checksum-verify", ""
    "3", "[2]", "storage", "write", """write_copies"": [ &copy ]"
    "4", "[3]", "query", "mark_retired", "
    | ""include_task_if"": {
    |    ""is_last"": true, 
    |    ""volume"": &volume 
    | }"

**Request Configuration**

.. csv-table::
    :header: "Element Id", "Config" 
    :widths: 3, 17

    "1", """restore_copy"": 2"

**Description**

This flow iterates over all artifacts on the volume:

1. The artifact is restored. The request config *restore_copy* property tells the system which copy to restore from. For example, if volume copy 1 is known to be defective then we might specify *restore_copy 2* to restore a good copy. (See :ref:`tasks-restore`).
2. The restored artifact's file checksums are verified.
3. The artifact is written. The *write_copies* property tells the system to write only to the same volume group as the volume specified by the API, making use of the &volume context variable. (See :ref:`tasks-write`).
4. The *mark_retired* query is run if we are on the last iteration, which means that all the artifacts have been successfully rewritten. This uses the &volume variable to specify that this volume should be marked "retired".

-------
migrate
-------

This flow is used for migrating all the artifacts on a storage volume to a new volume. For example due to storage media obsolescence.

API: **/volume/A10012L5/flow/migrate**

**Flow Element Configuration**

.. csv-table::
   :header: "Id", "Dependencies", "Task Type", "Task", "Config" 
   :widths: 5, 5, 5, 5, 20

    "1", "", "storage", "restore", ""
    "2", "[1]", "processing", "checksum-verify", ""
    "3", "[2]", "storage", "write", """write_copies"": [ &copy ]"
    "4", "[3]", "query", "mark_migrated", "
    | ""include_task_if"": {
    |    ""is_last"": true, 
    |    ""volume"": &volume 
    | }"

**Description**

This flow is executed for all artifacts on the volume:

1. The artifact is restored (from copy 1 by default). 
2. The artifact's file checksums are verified 
3. The artifact is written. The *write_copies* property tells the system to write only to the same volume group as the volume specified by the API, making use of the &volume context variable. (See :ref:`tasks-write`).
4. The *mark_migrated* query is run if we are on the last iteration, which means that all the artifacts have been successfully rewritten. This uses the &volume variable to specify that this volume should be marked "retired".
   
----
copy
----

This flow is used for writing an additional copy of all the artifacts of a certain type from a specified storage volume. For example, suppose we have been writing 3 copies 

API: **/volume/A10012L7/flow/copy**

**Flow Configuration** ::

    {
        "include_artifact_if": [
            {
                "artifactclassRegex": "photo-.*"
            }
        ]
    }

**Flow Element Configuration**

.. csv-table::
   :header: "Id", "Dependencies", "Task Type", "Task", "Config" 
   :widths: 5, 5, 5, 5, 20

   "1", "", "storage", "restore", ""
   "2", "[1]", "processing", "checksum-verify", ""
   "3", "[2]", "storage", "write", """copy"": 4"

**Description**

This flow is executed for all artifacts on the volume:

1. The artifact is restored. 
2. The artifact's file checksums are verified 
3. The artifact is written. The *mode migrate* configuration tells the system to write the artifact to the same volume group as the original volume alone, and mark the original artifact copy as ""migrated"".

--------------------------
partial file restore (PFR)
--------------------------

This flow is used for restoring a segment of a video file from a tape. This can be useful when storing very high resolution video where the file sizes are very large and there is a frequent need to retrieve only small subclips. 

A typical example would be a football match filmed by several cameras each creating a 2 hour video file. For broadcasting the match highlights we would just need a few seconds of the videos at the specific places where the key moments happened. Restoring the whole file from tape would be time consuming and would put a higher load on disk and network bandwidth resources when transferring it.

A PFR flow allows us to achieve this using the following steps:

1. Start with a video file id along with a start and end offset in seconds
2. Look up the corresponding file byte offsets (from an index file that was generated when the artifact was ingested)
3. Restore the specified byte range from tape
4. Get any metadata (header/footer) files needed for the next step (also generated at the time of ingest) 
5. Rewrap the restored byte range with any required metadata files to produce a valid video file
6. Convert the new video into a format useful for the end user to edit with. 

API: **/file/277532/flow/pfr**

**Flow Element Configuration**

.. csv-table::
   :header: "Id", "Dependencies", "Task Type", "Task", "Config" 
   :widths: 5, 5, 10, 10, 20

   "1", "", "processing", "pfr-preflight", """exports"": [startByte, endByte]" 
   "2", "[1]", "storage", "restore", """start_byte"": &startByte, ""end_byte"": &endByte, ""destination_path_id"""
   "3", "[2]", "processing", "pfr-rewrap", ""
   "4", "[3]", "processing", "convert-mov-dv50", ""

**Request Configuration**

.. csv-table::
    :header: "Element Id", "Config" 
    :widths: 3, 17

    "1", "
    | ""start_sec"": 1631,
    | ""end_sec"": 1646, 
    | ""metadata_path_id"": ""pfr-metadata"""

**Description**

1. The pfr-preflight task locates the PFR index file and any other metadata files using the *metadata_path_id* property and the file id. The task then uses the *start_sec* and *end_sec* values to look up the corresponding byte offsets from the retrieved index file and defines *startByte* and *endByte* export variables using the respective values.
2. The byte range is restored from tape. Note that the capability to do a byte range restore depends on whether the storage volume archive format implementation supports it. (Dwara's tar implementation supports byte range restore)
3. The artifact's file checksums are verified 
4. The artifact is written. The *mode migrate* configuration tells the system to write the artifact to the same volume group as the original volume alone, and mark the original artifact copy as ""migrated"".



"path_id", "string", "static", "ID of a local or remote directory path"
 artifacts. This can be used to specify the known good copy. Normally used in dynamic configurations only."
"start_offset", "int", "dynamic", "Start offset for range based file restore (units depend on task)" 
"end_offset", "int", "dynamic", "End offset for range based file restore (units depend on task)"
