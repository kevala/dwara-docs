.. _tasks:

=====
Tasks
=====

-------------
Storage Tasks
-------------

There are just two storage tasks: **write** and **restore**.

.. _tasks-write:

Write
+++++

The write task writes an artifact to one or more storage volumes. By default the artifact is written to all volumes configured in the *artifactclass_volume* mapping table. 

properties
~~~~~~~~~~

.. csv-table::
   :header: "Property", "Type", "Description"
   :widths: 10, 5, 25
 
   "skip_copies", "array (int)", "Skips writing the artifact to the specified copies"
   "write_copies", "array (int)", "Only writes the artifact to the specified copies" 
   "copy", "int", "Specifies the restored artifact copy for setting its status"
   "copy_status", "string", "Status which the artifact specified by the *restore_copy* property must be set to ( ""deleted"" | ""migrated"" )

.. _tasks-restore:

Restore
+++++++

The restore task restores an artifact or a file from a storage volume. Instead of the entire file, a byte range can be restored by specifying start and end byte offsets.

Properties
~~~~~~~~~~

.. csv-table::
   :header: "Property", "Type", "Description"
   :widths: 10, 5, 25

   "restore_copy", "int", "Which copy of the artifact to restore (default is copy 1)."
   "start_byte", "Start byte for range restore" 
   "end_byte", "End byte for range restore"

----------------
Processing Tasks
----------------

Processing tasks perform processing on certain types of files. They can be added as extensions to support the requirements of a particular implementation. However Dwara does have several core processing tasks to support its core operations. These are checksum-gen, checksum-verify, file-copy, file-delete, file-ignore. 

Processing tasks operate on logical units of one or more related files defined by a filetype. The keyword _all_ is a special filetype used to denote all files. Processing tasks can result in the creation of new artifacts. Such tasks must define an output filetype as well as an output artifactclass suffix. The artifactclass that should contain the new artifact is obtained by concatenating the input artifactclass name with the suffix. The task must generate file(s) that match the output filetype configuration, otherwise it will fail.

If the output artifactclass suffix is set to NULL then the processing task does not result in the creation of a new artifact. If the output artifactclass suffix is set to '' (empty string) then the output artifactclass is the same as the input artifactclass. In this case we also assume that the input and output artifacts are the same (so the sequence code extraction/assignment logic is skipped - see sequences)

Processing tasks can set properties dynamically which dependent tasks can access. The dependent task must be configured 

A new processing task can be defined by registering a Java class that implements the IProcessingTask interface, and adding a corresponding entry in the processingtask table. Note that the processingtask table does not contain entries for core processing tasks.

-------------
Utility Tasks
-------------

* Storage administration tasks such as initializing and finalizing volumes.
* File system operations such as moving or deleting files as part of a flow.

.. csv-table::
   :header: "Action", "Request", "Description"
   :widths: 7, 4, 25

   "scan", "no", "List and validate the files in the users' ingest directories. This is an important step to perform before doing an ingest."
   "list", "no", "List a set of artifacts or files based on some filter criteria." 
   "rename_staged", "yes", "Rename a staged artifact."
   "cancel", "yes", "Cancel a queued system request."
   "requeue", "yes", "Requeue a failed job. Simply resets the job's status to 'queued'. The admin should have addressed the reason for the job failure so that it should succeed in the subsequent run." 


